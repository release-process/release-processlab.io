---
title: "Conventions"
description: ""
summary: ""
date: 2024-02-16T15:19:20+01:00
lastmod: 2024-02-16T15:19:20+01:00
draft: false
menu:
  docs:
    parent: ""
    identifier: "conventions-1c2ff21e3fc084b8e2f618a0a9e360aa"
weight: 820
toc: true
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
All components of Release Process follow a set of conventions to ensure a consistent user experience and to simplify interoperability between Release Process components and CI/CD components from other sources.

## Terminology

Release Process uses the same technology as the official [CI/CD Component documentation of GitLab](https://docs.gitlab.com/ee/ci/components/). This means a *Component* is a single, includable pipeline configuration unit (single job, multiple jobs or just some declarations). A *Component* is contained inside a *Component Repository* which is versioned using Git tags (and GitLab Releases respectively). A *Component Repository* can contain multiple components (up to 10).

{{<callout type="info">}}
Although the official documentation limits the number of components per repository to 10, there seems to be no real limitation from a technical perspective. However, the number of listed components in the GitLab "CI/CD Catalog" might be limited to 10 per repository.
{{</callout>}}

For this documenation, we also use the terms **MUST**, **SHOULD**, **MAY** and **MUST NOT** as defined in [RFC 2119](https://tools.ietf.org/html/rfc2119).

## Component types

Currently Release Process defines two types of components: *Pipeline Components* and *Job Components*.

### Job Components

A *Job Component* is a reusable, single job definition that can be included in a pipeline or used as-is.

A *Job Component* is named by the main function that it provides (i.e. `go-test` if it runs `go test`).

Every *Job Component* specifies a single job which is named after the *Component Repository* and the *Job Component* itself: `<component-repository>:<job-component>`. This default name can be overridden by the user using an Input parameter (`name`).

#### Stages of Jobs

Every *Job Component* **MUST** specify the stage in which it should run. This is done using an input parameter `stage` with a sane default. The default value of `stage` **MUST** be defined and one of `build`, `test` or `deploy`. The user **MAY** override the default value with any valid stage name.

#### Input parameters

All *Job Components* accept the following input parameters:

| Name | Description | Default |
|------|-------------|---------|
| `stage` | The stage in which the job should run | one of `build`,`test` or `deploy` - whichever fits best according to the component author |
| `name` | The name of the job | `<component-repository>:<job-component>` |
| `dir` | The directory, the job should be executed in | `.` (project root) |

All input parameters **MUST** be documented in the component's README. (including the above mentioned parameters).

### Pipeline Components

A *Pipeline Component* combines multiple *Job Components* (usually of the same *Component Repository*) to create a complete, opinionated workflow.

A *Pipeline Component* is either called `pipeline` (default) or if multiple variants exist in the same *Component Repository*, it is named `pipeline-<variant>`.

## Component naming example

Let's assume we have a *Component Repository* for Go, which provides a single *Pipeline Component* and three *Job Components* that implement `go test`, `go build` and `golangci-lint`.

* The *Component Repository* would be named `golang`.
* The *Pipeline Component* would be named `pipeline` (as it is the default and only *Pipeline Component*).
* The *Job Component* would be named `go-test`, `go-build`, `golangci-lint`

Using the component could look like this:

**.gitlab-ci.yml**

```yaml
# to use the complete pipeline
include:
  - component: gitlab.com/release-process/golang/pipeline@v0.1.0

# alternatively, individual jobs could be included
include:
  - component: gitlab.com/release-process/golang/go-test@v0.1.0
  - component: gitlab.com/release-process/golang/go-build@v0.1.0
  - component: gitlab.com/release-process/golang/golangci-lint@v0.1.0
```

## Global Variables

Components **SHOULD** pass all needed parameters via `input` parameters of the component. However, sometimes this is not feasible, as values need to be calculated at runtime. In this case, components **CAN** use global variables to pass values as additional input. Global variables can also be used as outputs of component (by using the [dotenv report](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv) mechanism of GitLab CI).

### Global Variables naming

As all global variables are in a shared namespace, components should take extra care to prevent unintentional naming conflicts. Global variables **SHOULD** be prefixed with `RP_` where possible. The use of global variables **MUST** be documented in the component's README.

## Documentation

### Component Repository README

All Components **MUST** be documented in the README of the Component Repository. Each Component **MUST** list:

* all input parameters in a table (name, description, default)
* all variables in a table (name, description, default)
* all artifacts that are exported by the component including their expiration date (if applicable)

### Pipeline Graph

All pipelines **MUST** be documented with a [mermaid flowchart graph](https://mermaid.js.org/syntax/flowchart.html) using the following conventions:

* stages are represented as [subgraphs](https://mermaid.js.org/syntax/flowchart.html#subgraphs) containing the jobs that are part of the stage
* stages are labeled `stage: <stage-name>`
* stages are connected with arrows in the order they are executed
* jobs are represented as nodes with the job name as label
* jobs with a `needs` attribute are drawn as a [hexagon node](https://mermaid.js.org/syntax/flowchart.html#a-hexagon-node)
* jobs without a `needs` attribute are drawn as a regular node
* a job with `needs` is connected to the job it needs with an arrow

**Example**

The following pipeline code would generated the graph below:

```yaml
stages:
  - stage1
  - stage2

empty-needs-job:
  stage: stage1
  script:
    - echo "empty-needs-job"
  needs: []

regular-job:
  stage: stage1
  script:
    - echo "regular-job"

needs-job:
  stage: stage2
  script:
    - echo "needs-job"
  needs:
    - empty-needs-job

regular-job-2:
  stage: stage2
  script:
    - echo "regular-job-2"
```

```mermaid
flowchart TB

    subgraph stage1["stage: stage1"]
    direction TB
    job:empty-needs-job{{"empty-needs-job"}}
    job:regular-job["regular-job"]
    end

    subgraph stage2["stage: stage2"]
    direction TB
    job:needs-job{{"needs-job"}}
    job:regular-job-2["regular-job-2"]
    end

    job:empty-needs-job --> job:needs-job

    stage1 --> stage2
```
