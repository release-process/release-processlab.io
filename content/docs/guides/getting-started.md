---
title: "Getting Started"
description: "This guide will help you get started with the GitLab CI/CD components of Release Process."
summary: ""
date: 2024-02-15T10:57:58+01:00
lastmod: 2024-02-15T10:57:58+01:00
draft: false
menu:
  docs:
    parent: ""
    identifier: "getting-started-d80596a2c83aec0176c3dfbb6e8e7b71"
weight: 810
toc: true
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---

{{<callout context="note">}}
This guide is still work in progress.
{{</callout>}}

## Let's start simple

Let's assume you have a Go project and you want to use the GitLab CI/CD components of Release Process to build and test your project.

All you have to do is to create a `.gitlab-ci.yml` file in the root of your project and include the `release-process/go-binary/pipeline` component.

**.gitlab.ci.yml**

```yaml title=".gitlab-ci.yml"
include:
  - component: gitlab.com/release-process/golang/pipeline@v0.1.0
```

This will create a pipeline that lints, tests and builds your Go project.

The pipeline will consist of

* a `golang:golangci-lint` job that runs the `golangci-lint` linter
* a `golang:go-test` job that runs the `go test ./...` command
* a `golang:go-build` job that builds your project using `go build`.
