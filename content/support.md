---
title: "Get Support"
description: ""
summary: ""
date: 2024-02-15T18:45:10+01:00
lastmod: 2024-02-15T18:45:10+01:00
draft: false
type: "about"
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: true # false (default) or true
---
#### Community Support

If you have improvement ideas, encouter bugs or find documentation issues, please feel free to file an issue at the respective project on [GitLab](https://gitlab.com/release-process):

* [Web and Documentation issues](https://gitlab.com/release-process/release-process.gitlab.io/-/issues).
* For component specific issues, please use the respective project's issue tracker on [GitLab](https://gitlab.com/release-process).
