---
title: "Impressum - Legal Notice"
description: ""
summary: ""
date: 2024-02-15T16:26:00+02:00
lastmod: 2024-02-15T16:26:00+02:00
draft: false
type: "legal"
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
Information in accordance with §5 TMG

Stefan Hojer  
Altusrieder Straße 33  
87439 Kempten  
Germany

## Contact Information

Telephone: +49 176 24068150  
E-Mail: mail@stefanhojer.de

## VAT number

VAT indentification number ("USt-Id") in accordance with §27a of the German
VAT act

DE327317045

## Professional Indemnity Insurance Information

Markel Insurance SE  
Sophienstr. 26  
80333 München

Area covered by insurance: world wide

[Insurance details](https://www.exali.de/siegel/Stefan-Hojer-IT-Beratung)

## Disclaimer

### Accountability for links

Responsibility for the content of external links (to web pages of third
parties) lies solely with the operators of the linked pages. We checked the
links carefully and no violations were evident to us at the time of linking.
Should any legal infringement become known to us, we will remove the
respective link immediately.
