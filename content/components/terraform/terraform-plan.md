---
title: "Job: terraform-plan"
slug: "terraform-plan"
description: "run `terraform plan` on the specified directory and save the output plan.tfplan and plan.json"
summary: "run `terraform plan` on the specified directory and save the output plan.tfplan and plan.json"
date: 2024-04-29T23:12:37+02:00
lastmod: 2024-04-29T23:12:37+02:00
draft: false
weight: 800
toc: false
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
run `terraform plan` on the specified directory and save the output plan.tfplan and plan.json

## Usage

```yaml
include:
  - component: gitlab.com/release-process/terraform/terraform-plan@0.1.0
```

## Inputs

|Name|Description|Default|
|---|---|---|
| **args** | additional arguments to pass to the `terraform plan` command | `` |
| **dir** | the directory to build | `.` |
| **init_args** | additional arguments to pass to the `terraform init` command | `-lockfile=readonly` |
| **name** | The name of the job application | `terraform:terraform-plan` |
| **stage** | stage of the job | `build` |
| **version** | terraform version to use for building | `1.8` |


## Artifacts

- `$[[ inputs.dir ]]/plan.tfplan`
- `$[[ inputs.dir ]]/plan.json`

**Expires in:** `1 week`.
