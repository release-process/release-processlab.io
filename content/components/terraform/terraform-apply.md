---
title: "Job: terraform-apply"
slug: "terraform-apply"
description: "run `terraform apply` on the specified directory"
summary: "run `terraform apply` on the specified directory"
date: 2024-04-29T23:12:37+02:00
lastmod: 2024-04-29T23:12:37+02:00
draft: false
weight: 800
toc: false
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
run `terraform apply` on the specified directory

## Usage

```yaml
include:
  - component: gitlab.com/release-process/terraform/terraform-apply@0.1.0
```

## Inputs

|Name|Description|Default|
|---|---|---|
| **args** | additional arguments to pass to the `terraform apply` command | `plan.tfplan` |
| **dir** | the directory to build | `.` |
| **init_args** | additional arguments to pass to the `terraform init` command | `-lockfile=readonly` |
| **name** | The name of the job application | `terraform:terraform-apply` |
| **stage** | stage of the job | `deploy` |
| **version** | terraform version to use for building | `1.8` |
