---
title: "Pipeline: Terraform"
slug: "pipeline"
description: "Pipeline for Terraform modules. This pipeline will run `terraform fmt`, `terraform plan` and `terraform apply`."
summary: "Pipeline for Terraform modules. This pipeline will run `terraform fmt`, `terraform plan` and `terraform apply`."
date: 2024-04-29T23:12:37+02:00
lastmod: 2024-04-29T23:12:37+02:00
draft: false
weight: 800
toc: false
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
Pipeline for Terraform modules. This pipeline will run `terraform fmt`, `terraform plan` and `terraform apply`.

## Flowchart `apply_mode=manual` or `apply_mode=auto-approve`

```mermaid
flowchart TD

classDef optional stroke-dasharray: 5 5

job:terraform-plan["terraform-plan"]
job:terraform-fmt{{"terraform-fmt"}}
job:terraform-apply["terraform-apply"]

subgraph stage:build["stage: build"]
job:terraform-plan
end

subgraph stage:test["stage: test"]
job:terraform-fmt
end

subgraph stage:deploy["stage: deploy"]
job:terraform-apply
end

stage:build --> stage:test
stage:test --> stage:deploy
```

## Flowchart `apply_mode=auto-approve-non-destructive`

```mermaid
flowchart TD

classDef optional stroke-dasharray: 5 5

job:terraform-plan["terraform-plan"]
job:terraform-fmt{{"terraform-fmt"}}
job:terraform-apply:generate["terraform-apply:generate"]
job:terraform-apply:trigger["terraform-apply:trigger"]

subgraph stage:build["stage: build"]
job:terraform-plan
end

subgraph stage:test["stage: test"]
job:terraform-fmt
end

subgraph stage:deploy["stage: deploy"]
job:terraform-apply:generate
job:terraform-apply:trigger

job:terraform-apply:generate --> job:terraform-apply:trigger
end

stage:build --> stage:test
stage:test --> stage:deploy

subgraph child:terraform-apply["child pipeline: terraform-apply"]
child:terraform-apply:terraform-apply["terraform-apply"]
child:terraform-apply:gitlab-issue-361574["gitlab-issue-361574"]
end

job:terraform-apply:trigger -.- child:terraform-apply
```

## Usage

```yaml
include:
  - component: gitlab.com/release-process/terraform/pipeline@0.1.0
```

## Inputs

|Name|Description|Default|
|---|---|---|
| **apply_mode** | The mode to use when applying changes. See also: [apply_mode Options](#apply_mode-options) | `manual` |
| **dir** | the directory to build | `.` |
| **name** | The name to prefix all job names with | `terraform` |
| **terraform_init_args** | additional arguments to pass to the `terraform init` command | `-lockfile=readonly` |
| **terraform_version** | The version of Terraform to use | `1.8` |

### apply_mode Options

|Name|Description|
|---|---|
| **manual** | Requires manual approval before applying changes. |
| **auto-approve** | Automatically applies changes without requiring approval. |
| **auto-approve-non-destructive** | Automatically applies changes without requiring approval, but only if the plan is non-destructive. |