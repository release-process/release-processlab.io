---
title: "Job: go-build"
slug: "go-build"
description: "run `go build` and export outputs as artifacts"
summary: "run `go build` and export outputs as artifacts"
date: 2024-04-29T23:12:37+02:00
lastmod: 2024-04-29T23:12:37+02:00
draft: false
weight: 800
toc: false
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
run `go build` and export outputs as artifacts

## Usage

```yaml
include:
  - component: gitlab.com/release-process/golang/go-build@0.1.0
```

## Inputs

|Name|Description|Default|
|---|---|---|
| **args** | additional arguments to pass to the `go build` command | `./...` |
| **dir** | the directory to build | `.` |
| **name** | The name of the job application | `golang:go-build` |
| **output** | the output file/folder | `out/` |
| **stage** | stage of the job | `build` |
| **version** | go version to use for building | `1.22` |


## Variables

|Name|Description|Default|
|---|---|---|
| **CGO_ENABLED** | wether to use cgo or not | `0` |
| **GOARCH** | target architecture (empty means same as build platform) | `""` |
| **GOOS** | target operating system (empty means same as build platform) | `""` |
| **OUTPUT** | output file/folder | `$[[ inputs.output ]]` |

## Artifacts

- `${OUTPUT}`

**Expires in:** `1 week`.
