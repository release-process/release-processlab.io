---
title: "Job: golangci-lint"
slug: "golangci-lint"
description: "run `golangci-lint` on your Go code"
summary: "run `golangci-lint` on your Go code"
date: 2024-04-29T23:12:37+02:00
lastmod: 2024-04-29T23:12:37+02:00
draft: false
weight: 800
toc: false
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
run `golangci-lint` on your Go code

## Usage

```yaml
include:
  - component: gitlab.com/release-process/golang/golangci-lint@0.1.0
```

## Inputs

|Name|Description|Default|
|---|---|---|
| **args** | additional arguments to pass to the `go test` command | `./...` |
| **dir** | the directory to build | `.` |
| **name** | The name of the job application | `golang:golangci-lint` |
| **stage** | stage of the job | `test` |
| **version** | golangci-lint version to use for building | `1.54` |
