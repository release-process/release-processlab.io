---
title: "Pipeline: Golang"
slug: "pipeline"
description: "Pipeline for Golang projects. This pipeline will run `golangci-lint`, `go generate` (optional), `go build`, and `go test`."
summary: "Pipeline for Golang projects. This pipeline will run `golangci-lint`, `go generate` (optional), `go build`, and `go test`."
date: 2024-04-29T23:12:37+02:00
lastmod: 2024-04-29T23:12:37+02:00
draft: false
weight: 800
toc: false
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
Pipeline for Golang projects. This pipeline will run `golangci-lint`, `go generate` (optional), `go build`, and `go test`.

## Flowchart

```mermaid
flowchart TD

classDef optional stroke-dasharray: 5 5

job:go-generate{{"go-generate"}}
job:go-build{{"go-build"}}
job:go-test{{"go-test"}}
job:golangci-lint{{"golangci-lint"}}

class job:go-generate optional
class job:go-test optional

subgraph stage:build["stage: build"]
job:go-generate --> job:go-build
end

subgraph stage:test["stage: test"]
job:go-generate --> job:go-test
job:go-generate --> job:golangci-lint
end

stage:build --> stage:test
```


## Usage

```yaml
include:
  - component: gitlab.com/release-process/golang/pipeline@0.1.0
```

## Inputs

|Name|Description|Default|
|---|---|---|
| **dir** | the directory to build | `.` |
| **generate** | Whether to run go generate | `false` |
| **go_version** | The version of Go to use | `1.22` |
| **golangci_lint_version** | The version of golangci-lint to use | `1.54` |
| **name** | The name to prefix all job names with | `golang` |
